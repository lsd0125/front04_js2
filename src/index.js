const express = require('express');

express.shin = 'abc';

const url = require('url');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer({dest:'tmp_uploads/'});
const fs = require('fs');
const session = require('express-session');
const moment = require('moment-timezone');
const mysql = require('mysql');
const bluebird = require('bluebird');
const cors = require('cors');
const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'mytest'
});
db.connect();

bluebird.promisifyAll(db);

const app = express();
//const urlencodedParser = bodyParser.urlencoded({extended: false});
// app.use(cors());
const whitelist = ['http://localhost:5000', undefined,];
const corsOptions = {
    credentials: true,
    origin: function(origin, callback){
        console.log('origin: ' + origin);

        if(whitelist.indexOf(origin)>=0){
            callback(null, true); //允許
        } else {
            //callback(new Error('EEEEEEEEError'));
            callback(null, false); //不允許
        }
    }
};
app.use(cors(corsOptions));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'akjdfaki',
    cookie: {
        maxAge: 1200000,
    }
}));


app.set('view engine', 'ejs');

app.use(express.static('public'));

// routes 路由

app.get('/', (req, res)=>{

    res.render('home', {name:'Shinder Lin', a:123});
});

app.get('/b.html', (req, res)=>{
    res.send(`<h2>
        Hello world!
        </h2>`);
});
app.get('/sales01', (req, res)=>{
    const sales = require('./../data/sales01');
    //res.send(JSON.stringify(sales));
    //res.json(sales);
    res.render('sales01', {
       my_var:  sales
    });
});

app.get('/try-qs', (req, res)=>{
    const urlParts = url.parse(req.url, true);
    console.log(urlParts);

    res.render('try-qs', {
        query:  urlParts.query
    });
});

app.get('/try-post-form', (req, res)=>{
    res.render('try-post-form');
});
app.get('/try-post-form/123', (req, res)=>{
    res.render('try-post-form');
});
app.post('/try-post-form', (req, res)=>{
    res.render('try-post-form', req.body);

    //res.send(JSON.stringify(req.body));
});


app.get('/try-post-form2', (req, res)=>{
    res.send('get: try-post-form2');
});
app.post('/try-post-form2', (req, res)=>{
    res.json(req.body);
});
app.put('/try-post-form2', (req, res)=>{
    res.send("PUT: try-post-form2");
});

app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    if(req.file && req.file.originalname){
        console.log(req.file);

        switch(req.file.mimetype){
            case 'image/png':
            case 'image/jpeg':
                fs.createReadStream(req.file.path)
                    .pipe(
                        fs.createWriteStream('public/img/' + req.file.originalname)
                    );

                res.send('ok');
                break;
            default:
                return res.send('bad file type');
        }
    } else {
        res.send('no uploads');
    }
});

app.get('/my-params1/:action?/:id?', (req, res)=>{
    res.json(req.params);
});
app.get('/my-params2/*?/*?', (req, res)=>{
    res.json(req.params);
});

app.get(/^\/09\d{2}\-?\d{3}\-?\d{3}$/, (req, res)=>{
    let str = req.url.slice(1);
    str = str.split('?')[0];
    str = str.split('-').join('');
    res.send('手機: '+ str);
});

const admin1 = require(__dirname + '/admins/admin1');
admin1(app);

app.use( require(__dirname + '/admins/admin2') );
app.use('/admin3', require(__dirname + '/admins/admin3') );

app.use('/address-book', require(__dirname + '/address_book') );

app.get('/try-session', (req, res)=>{
    req.session.my_views = req.session.my_views || 0;
    req.session.my_views++;

    res.json({
        aa: 'hello',
        'my views': req.session.my_views
    });
});
app.get('/try-moment', (req, res)=>{
    const fm = 'YYYY-MM-DD HH:mm:ss';
    const mo1 = moment(req.session.cookie.expires);
    const mo2 = moment(new Date());
    res.contentType('text/plain');
    res.write(req.session.cookie.expires.toString() + "\n");
    res.write(req.session.cookie.expires.constructor.name + "\n");
    res.write(new Date() + "\n");
    res.write(mo1.format(fm) + "\n");
    res.write(mo2.format(fm) + "\n");
    res.write('倫敦:' + mo1.tz('Europe/London').format(fm) + "\n");
    res.write(mo2.tz('Asia/Tokyo').format(fm) + "\n");
    res.end('');
});

app.get('/try-db', (req, res)=> {
    const sql = "SELECT * FROM `address_book` WHERE `name` LIKE ? ";
    db.query(sql, [ "%李小明%" ], (error, results, fields)=>{
        console.log(error);
        console.log(results);
        console.log(fields);
        //res.json(results);

        for(let r of results){
            r.birthday2 = moment(r.birthday).format('YYYY-MM-DD');
        }


        res.render('try-db',{
            rows: results
        });
    });
    //res.send('ok');
});

app.get('/try-db2/:page?', (req, res)=> {
    let page = req.params.page || 1;
    let perPage = 5;
    const output = {};

    db.queryAsync("SELECT COUNT(1) total FROM `address_book`")
        .then(results=>{
            //res.json(results);
            output.總筆數 = results[0].total;
            return db.queryAsync(`SELECT * FROM address_book LIMIT ${(page-1)*perPage}, ${perPage}`);
        })
        .then(results=>{
            output.rows = results;
            res.json(output);
        })
        .catch(error=>{
            console.log(error);
            res.send(error);
        });
});

app.get('/try-session2', (req, res)=> {
    req.session.views = req.session.views || 0;
    req.session.views++;

    res.json({views: req.session.views});
});




app.use((req, res)=>{
    res.type('text/plain');
    res.status(404);
    res.send('找不到頁面');
});

app.listen(3000, ()=>{
   console.log('server started 3000');
});







